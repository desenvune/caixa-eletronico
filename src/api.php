<?php
session_start();
/**
 * Arquivo respons�vel pelo tratamento das requisi��es feitas pela aplica��o
 */
//include_once 'Conta.php';


$action = filter_input(INPUT_GET, 'action');
$retorno = array();
switch ($action) {

    /**
     * Instru��es da a��o iniciaConta:
     * 1 - Instanciar um objeto da classe Conta
     * 2 - Deposita o valor recebido via $_GET na conta
     * 3 - Guardar o objeto Conta na sess�o para utiliz�-lo depois (no deposito e no saque)
     * 4 - Enviar o saldo para cliente
	 *
	 * DICA! 
	 * Como guardar e recuperar um objeto da sess�o?
     *           
     *        $obj = new MyClass();
	 *        $obj->setSaldo(10);
     *        $_SESSION['obj'] = serialize($obj);
	 *
     */
    case 'iniciaConta':
	
		$valor = filter_input(INPUT_POST, 'valor');
		
		//$obj = new Conta();
	    //$obj->setSaldo($valor);
        //$_SESSION['obj'] = serialize($obj);

        $retorno['action'] = 'atualiza-saldo'; 
        $retorno['valor'] = $valor; // retornar saldo da conta
        break;
		
	/**
     * Instru��es da a��o deposito:
     * 1 - Recupere o objeto Conta
     * 2 - Deposite na conta o valor passado no post
     * 3 - Guardar o objeto Conta na sess�o para utiliz�-lo depois
     * 4 - Enviar o saldo para cliente
	 *
	 * DICA! 
	 * Como ecuperar um objeto da sess�o?
     *           
     *     		$obj = unserialize($_SESSION['obj']);
	 *		  $saldo = $obj->getSaldo();
     *       
     *
     */
    case 'deposito':
	
		//$obj = unserialize($_SESSION['obj']);
		
		//$saldo = $obj->getSaldo();
	
		$retorno['action'] = 'atualiza-saldo';
        $retorno['valor'] = $saldo;
        break;
        break;

    /**
     * Instru��es da a��o saque:
     * 1 - Recupere o objeto Conta
     * 2 - Sacar da conta o valor passado no post
     * 3 - Guardar o objeto Conta na sess�o para utiliz�-lo depois
     * 4 - Enviar o saldo para cliente
     */
    case 'saque':

        break;

   
    default:
        break;
}
echo json_encode($retorno);

